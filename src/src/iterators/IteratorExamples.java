package iterators;

import java.util.*;

public class IteratorExamples {
    public static void main(String[] args) {
        List<String> names = new ArrayList<>();
        names.add("Angela");
        names.add("Kuba");
        names.add("James");
        names.add("Zordon");

        // wypisanie
        // 1. Standard for
//        for (int i = 0; i < names.size(); i++) {
//            System.out.println(names.get(i));
//        }

        // 2. Z użyciem streama
//        names.forEach(System.out::println);

        // 3. Z wykorzystaniem iteratora
//        Iterator<String> iterator = names.listIterator();
//        while (iterator.hasNext()) {
//            String name = iterator.next();
//            System.out.println(name);
//        }

        // Usuwanie elementów z listy podczas jej iterowania za pomocą fora
        for (int i=0; i < names.size(); i++) {
            String name = names.get(i);
            if (name == "Zordon") {
                names.remove(i);
            }
            System.out.println(names.get(i));
        }

        // Usuwanie elementów wykorzystując iterator
        ListIterator<String> listIterator = names.listIterator();
        while (listIterator.hasNext()) {
            String name = listIterator.next();
            if (name == "Zordon") {
                listIterator.remove();
            }
            if (!listIterator.hasNext()) {
                System.out.println("Nie ma więcej elementów");
            }
        }
    }
}
