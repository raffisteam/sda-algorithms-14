package exercises.lambda.introduction.materials;

import java.util.ArrayList;
import java.util.List;

/**
 * @author marek.sobieraj on 2018-03-16
 */
public class RobotGroup {

    public static List<Robot> getRobotGroup() {
        List<Robot> robotGroup = new ArrayList<>();
        robotGroup.add(new Robot("AWT", true, false, true));
        robotGroup.add(new Robot("Wicket", false, false, false));
        robotGroup.add(new Robot("Angular", true, true, true));
        robotGroup.add(new Robot("Iridium", false, true, true));
        robotGroup.add(new Robot("Spring", false, true, false));
        robotGroup.add(new Robot("Hibernate", true, true, false));
        return robotGroup;
    }

    public static void main(String[] args) {

        List<Robot> robotGroup = getRobotGroup();
//        print(robotGroup, new CheckIfDancer());

        print(robotGroup, robot -> robot.isRunner());
    }

    public static void print(List<Robot> robotList, CheckSport checker) {
        for (Robot robot : robotList) {
            if (checker.check(robot)) {
                System.out.println(robot);
            }
        }
        System.out.println();
    }
}
