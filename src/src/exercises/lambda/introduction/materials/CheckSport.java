package exercises.lambda.introduction.materials;

/**
 * @author marek.sobieraj on 2018-03-16
 */
public interface CheckSport {
    boolean check(Robot robot);
}
