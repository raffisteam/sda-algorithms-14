package exercises.lambda.introduction.materials;

/**
 * @author marek.sobieraj on 2018-03-16
 */
public class CheckIfDancer implements CheckSport {
    @Override
    public boolean check(Robot robot) {
        return robot.isDancer();
    }
}
