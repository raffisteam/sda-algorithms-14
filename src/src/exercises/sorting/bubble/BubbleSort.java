package exercises.sorting.bubble;

import util.CalcTime;
import util.DataSet;

import java.util.Arrays;

/**
 * Zadanie:
 * Zaimplementuj algorytm sortowania bąbelkowego.
 *
 * @author marek.sobieraj on 2018-03-12
 */
public class BubbleSort {

    private static final CalcTime CALC_TIME = new CalcTime();

    public static void main(String[] args) {
        Integer[] array = DataSet.getData();

        CALC_TIME.start();
        bubbleSort(array);
        CALC_TIME.stop();

        DataSet.compare(array);
        CALC_TIME.display();

        Arrays.stream(array).forEachOrdered((value -> System.out.print(value + " ")));
    }

    private static void bubbleSort(Integer array[]) {
        for (int i = array.length; i >= 0; i--) {
            for (int j = 0; j < i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    swap(j, j + 1, array);
                }
            }
        }
    }

    private static void swap(int i, int j, Integer[] array) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
