package exercises;

import java.util.Scanner;

/**
 * Znaleźć największy wspólny dzielnik dla dwóch podanych liczb
 * wykorzystując rekurencję.
 *
 * @author marek.sobieraj on 2018-03-08
 */
public class Exercise13 {

    public static void main(String[] args) {

        int firstNumber;
        int secondNumber;
        try (Scanner scanner = new Scanner(System.in)) {
            System.out.println("Podaj pierwszą liczbę: ");
            firstNumber = scanner.nextInt();
            System.out.println("Podaj drugą liczbę: ");
            secondNumber = scanner.nextInt();

            int nwd = nwd(firstNumber, secondNumber);
            System.out.println("NWD dla " + firstNumber + " i " + secondNumber + " wynosi " + nwd);
        }
    }

    private static int nwd(int firstNumber, int secondNumber) {
        // TODO:
        return -1;
    }
}
