package together;

public class Exercise2 {
    public static void main(String[] args) {
        System.out.println("Podaj 10 liczb:");
        int[] numbers = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        printInfo(numbers);

        for (int j = 0; j < numbers.length / 2; j++) {
            int element = numbers[numbers.length - 1 - j];
            numbers[numbers.length - 1 - j] = numbers[j];
            numbers[j] = element;
        }

        printInfo(numbers);


    }

    public static void printInfo(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            System.out.printf("array [%d] = %d\n", i, numbers[i]);
        }
    }
}
